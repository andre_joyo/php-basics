<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <style>
        .red{
            color: red;
        }
        .blue{
            color: blue;
        }
    </style>
</head>
<body>
    <?php
        //Mostrar un mensaje por pantalla dentro de la etiqueta <h1> de HTML
        echo "<h1>Mensaje</h1>";
        /*Mostrar una lista de nombres mediante el uso de las etiquetas <ul> y <li>. 
        Estos nombres estarán almacenados dentro de una variable de tipo array / matriz, por lo que deberás
        recorrer dicha variable para imprimir un elemento <li> por cada uno de ellos.*/
        $arr_m=["abc", "cde", "efg"];
        $arr_n=["mnp", "npo", "poq"];
        $arr_p=["321", "654", "987"];
        $arr_x=[$arr_m, $arr_n, $arr_p];
        foreach ($arr_x as $key => $value) {
            echo "<ul>";
            foreach ($arr_x[$key] as $key_new => $value_new) {
                echo "<li>".$value_new."</li>";
            }
            echo "</ul>";
        }
        /*Crear una función que dado un nombre y un apellido ( que recibirá como parámetros ) muestre un mensaje de bienvenida de
        tipo “Hola Nombre Apellido, bienvenid@“. Es importante que aparte de definir la función la ejecutes en el script para que
        este mensaje se muestre por pantalla, por lo que deberás asegurarte que al invocarla le asignas los parámetros necesarios.*/
        function greeting($name, $l_name){
            echo "<p>Hola $name $l_name, bienvenido.</p>";
        }
        greeting("Nombre", "Apellido");
        //Mostrar la fecha actual
        $now=getdate();
        $today=$now["mday"]."-".$now["month"]."-".$now["year"];
        echo "Today is ".$today;
        //Si el día actual es par, mostrarás la fecha de color roja, si por el contrario es impar, se mostrará en color azul.
        if ($now%2==0) {
            echo "<p class='red'>Today is ".$today."</p>";
        } else{
            echo "<p class='blue'>Today is ".$today."</p>";
        }
        /*Mostrarás una imagen dependiendo de la estación de la fecha actual. No te preocupes demasiado por la imagen, lo importante
        es que tu script sea capaz de mostrar una determinada imagen dependiendo de la estación, además, mostrarás una cadena de texto*/
        $leap;
        function leap_year($year){
            global $leap;
            echo "year: ".$year."<br>";
            if ($year%4==0) {
                if ($year%100==0) {
                    if ($year%400==0) {
                        $leap=TRUE;                                            
                    } else {
                        $leap=FALSE;                    
                    }
                } else {
                    $leap=TRUE;                    
                }
            } else{
                $leap =FALSE;
            }
        }
        leap_year($now["year"]);
        if ($leap) {
            if ($now["yday"]<92) {
                echo "<img src='https://ep01.epimg.net/cat/imagenes/2018/06/03/economia/1528027606_777876_1528027687_noticia_normal.jpg' alt='A station' height='182' width='182'><figcaption>A station</figcaption>";
            } elseif($now["yday"]<184){
                echo "<img src='https://www.lavanguardia.com/r/GODO/LV/p5/WebSite/2018/03/07/Recortada/img_mbigas_20180326-095732_imagenes_lv_terceros_foto-kCIG-U44132972059510D-992x558@LaVanguardia-Web.jpg' alt='B station' height='182' width='182'><figcaption>B station</figcaption>";
            } elseif($now["yday"]<276){
                echo "<img src='https://cflvdg.avoz.es/default/2015/03/29/00121427660161753131409/Foto/v30c1f1.jpg' alt='C station' height='182' width='182'><figcaption>C station</figcaption>";
            } else {
                echo "<img src='https://imagenes.heraldo.es/files/image_990_v3/uploads/imagenes/2018/07/16/_Canfranc9_9f54c00a.jpg' alt='D station' height='182' width='182'><figcaption>D station</figcaption>";
            }
        } else{
            if ($now["yday"]<93) {
                echo "<img src='https://ep01.epimg.net/cat/imagenes/2018/06/03/economia/1528027606_777876_1528027687_noticia_normal.jpg' alt='A station' height='182' width='182'><figcaption>A station</figcaption>";
            } elseif($now["yday"]<185){
                echo "<img src='https://www.lavanguardia.com/r/GODO/LV/p5/WebSite/2018/03/07/Recortada/img_mbigas_20180326-095732_imagenes_lv_terceros_foto-kCIG-U44132972059510D-992x558@LaVanguardia-Web.jpg' alt='B station' height='182' width='182'><figcaption>B station</figcaption>";
            } elseif($now["yday"]<277){
                echo "<img src='https://cflvdg.avoz.es/default/2015/03/29/00121427660161753131409/Foto/v30c1f1.jpg' alt='C station' height='182' width='182'><figcaption>C station</figcaption>";
            } else {
                echo "<img src='https://imagenes.heraldo.es/files/image_990_v3/uploads/imagenes/2018/07/16/_Canfranc9_9f54c00a.jpg' alt='D station' height='182' width='182'><figcaption>D station</figcaption>";
            }
        }
    ?>
</body>
</html>