<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Maths</title>
</head>
<body>
    <?php
        //Define una variable cuyo valor sea el resultado de la función que retorna un valor absoluto
        $var=abs(-3);
        echo $var."<br>";
        //Define una variable cuyo valor sea el resultado de la función que retorna un valor redondeado al siguiente entero más alto
        $var=ceil(75.95);        
        echo $var."<br>";
        //Define una variable cuyo valor sea el resultado de la función que retorna el valor mas alto de una serie de valores que se reciben por parámetro
        $var=max(7, 34, 12, 8, 543);
        echo $var."<br>";
        //Define una variable cuyo valor sea el resultado de la función que retorna el valor mas bajo de una serie de valores que se reciben por parámetro
        $var=min(2, 3, 1, 6, 7);
        echo $var."<br>";
        //Define una variable cuyo valor sea el resultado de la función que retorna un número aleatorio
        $var=rand(0, 100);
        echo $var."<br>";
?>
</body>
</html>