<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>POO</title>
</head>
<body>
    <?php
        /*Crea almenos 4 clases. Cada clase debe de contener métodos y propiedades con sentido
        Deberás hacer uso de constructores y destructores propios*/
        class animal{
            public $current_animal;
            public function __construct($anim, $nam){
                $this->current_animal=$anim;
                $this->current_name=$nam;
                echo "This animal is a ".$this->current_animal."<br>";
            }
            public function do_something(){
                echo "The ".$this->current_animal." does something <br>";            
            }
            public function __destruct(){
                echo "The ".$this->current_animal." called ".$this->current_name." finished <br>";
            }
        }
        class intermediate extends animal{
            protected $animal;
            public function __construct($anim, $nam){
                parent::__construct($anim, $nam);
            }            
            public function __destruct(){
                parent::__destruct();
                echo "The ".$this->animal." finished <br><br>";
            }
        }
        class cat extends intermediate{
            public function __construct($anim, $nam){
                parent::__construct($anim, $nam);
                $this->animal=$anim;
                echo "The ".$this->animal." meows <br>";            
            }            
            public function __destruct(){
                parent::__destruct();
            }
        }
        class dog extends intermediate{
            public function __construct($anim, $nam){
                parent::__construct($anim, $nam);
                $this->animal=$anim;
                echo "The ".$this->animal." barks <br>";
            }
            public function __destruct(){
                parent::__destruct();
            }
        }
        //Instancia cada uno de los objetos y realiza ejemplos prácticos que validen tus conocimientos
        $tom=new cat("cat", "Tom");
        $tom->do_something();
        echo "Tom es un ".$tom->current_animal."<br><br>";
        
        $beethoven=new dog("dog", "Beethoven");
        $beethoven->do_something();
        echo "Beethoven es un ".$beethoven->current_animal."<br><br>";
        
        //Haz uso de la clonación de objetos
        $another_beethoven=clone $beethoven;
        $another_beethoven->__construct("dog", "Beethoven 2");
        $another_beethoven->do_something();
        echo "Beethoven 2 es un ".$another_beethoven->current_animal."<br><br>";
        
        //Haz uso de métodos para convertir el objeto a string        
        ob_start();
        var_dump($tom);
        $str_of_object = ob_get_clean();
        echo $str_of_object.", length: ".strlen($str_of_object);
    ?>
</body>
</html>