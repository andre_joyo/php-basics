<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Arrays</title>
</head>
<body>
    <?php
        //Define un array simple compuesto por cadenas de texto
        $arr=["a", "edrf", "ed"];
        //Define un array simple compuesto por números enteros y números decimales
        $arr=[12, 344, 89.34];
        //Define un array multidimensional
        for ($i=0; $i < 5; $i++) { 
            for ($j=0; $j < 5; $j++) { 
                $array[$i][$j]=rand()%100;
            }
        }
        var_dump($array);
        //Ejecuta la función que permita obtener la longitud de un array
        echo count($array);
        echo "<br>";
        //Ejecuta la función que permita obtener la combinación de dos arrays
        $arr_merg=array_merge($array[0], $array[1]);
        var_dump($arr_merg);
        echo "<br>";
        //Ejecuta la función que dado un array devuelva el último elemento del mismo
        var_dump (end($array));
        echo "<br>";
        //Ejecuta la función que dado un array agregue un nuevo elemento al array en cuestión
        array_push($array, "drty");
        var_dump($array);
    ?>
</body>
</html>