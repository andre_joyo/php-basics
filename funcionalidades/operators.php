<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Operators</title>
</head>
<body>
    <?php
        $a=random_int(0,1000);
        $b=random_int(0,1000);
        $c=random_int(0,1000);
        $div=[];
        for ($i=1; $i <= $c; $i++) { 
            if ($c%$i==0) {
                $div[count($div)]=$i;
            }
        }
        $d=$div[random_int(0, count($div)-1)];
        $e=random_int(1,12);
        $resultado=$a+$b*$c/$d;
        //Crea un ejemplo de uso para los operadores aritméticos: +, -, *, /, y %
        echo "El resultado de operar \"$a+$b*$c/$d\" es ".$resultado;
        if (($resultado)%$e==0) {
            echo " y es múltiplo de $e <br>";
        } else{
            echo " y no es múltiplo de $e <br>";
        }
        //Crea un ejemplo de uso para los operadores de comparación: ==, !=, <, >, <=, >=
        if ($a==$b && $a!=$c) {
            echo "$b!=$c <br>";
        } elseif($a==$b && $a==$c){
            echo "$a=$c <br>";
        } else{
            echo "$a!=$b <br>";
        }

        if ($a<$b && $c>$b) {
            echo "$a<$c <br>";
        } elseif ($a<$b) {
            echo "$c<=$b <br>";
        }else {
            echo "$a>=$b <br>";
        }

        if ($a<=$b && $b<=$a) {
            echo "$a=$b <br>";
        } else{
            echo "$a y $b son diferentes <br>";
        }
        //Crea un ejemplo de uso para los operadores lógicos: &&; ||; ! (NOT); Xor  
        $bool_a=true;
        $bool_b=false;
        if ($bool_a && !$bool_b) {
            echo "verdadero <br>";
        }  
        if ($bool_a || $bool_b) {
            echo "verdadero <br>";
        }
        if ($bool_a xor $bool_b) {
            echo "verdadero <br>";
        }
    ?>
</body>
</html>
