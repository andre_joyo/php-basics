<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Strings</title>
</head>
<body>
    <?php
    $var="24";
        //Imprimir una cadena de texto
        echo "Cadena";
        //Imprimir una cadena de texto que no interprete variables
        echo "mi variable \$var";
        //Imprimir una cadena de texto que interprete variables
        echo "mi variable $var";
        //Concatenar en una cadena de texto una variable declarada previamente
        echo "texto ".$var." más texto";
        //Ejecuta la función que permite reemplazar texto en una cadena ( sensible a mayúsculas / minúsculas )
        $cont = str_replace("%search%", "reemplazo", "a reemplazar: '%search%'");
        //Ejecuta la función que permite reemplazar texto en una cadena ( sin que tenga en cuenta mayúsculas / minúsculas )
        $cont = str_ireplace("%search%", "reemplazo", "a reemplazar: '%search%'");
        //Ejecuta la función que permite escribir N veces un texto
        str_repeat($cont,$var);
        //Ejecuta la función que permita obtener la longitud de una cadena de texto
        strlen($cont);
        //Ejecuta la función que permita obtener la posición de la primera ocurrencia de un texto dentro de una cadena de texto
        strpos($str, $findme);
        //Ejecuta la función que permita transformar a mayúsculas una cadena de texto
        strtoupper($str);
        //Ejecuta la función que permita transformar a minúsculas una cadena de texto
        strtolower($str);
        //Ejecuta la función que permita obtener una subcadena de texto a partir de la posición
        substr($str, $pos);
    ?>
</body>
</html>