<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Types</title>
</head>
<body>
    <?php
        class object_0
        {
            function hacer_algo()
            {
                echo "Haciendo algo."; 
            }
        }
        //boolean
        $var=true;
        echo "boolean: ";
        var_dump($var);
        echo "<br>";
        //integer
        $var=2;
        echo "integer: ";
        var_dump($var);
        echo "<br>";
        //float
        $var=4.5;
        echo "float: ";
        var_dump($var);
        echo "<br>";
        //string
        $var="abc";
        echo "string: ";
        var_dump($var);
        echo "<br>";
        //array
        $var=["a", "b", "c"];
        echo "array: ";
        var_dump($var);
        echo "<br>";
        //object
        $var= new object_0;
        echo "object: ";
        var_dump($var);
        echo "<br>";
        //NULL
        $var=NULL;
        echo "NULL: ";
        var_dump($var);
        echo "<br>";
?>
</body>
</html>