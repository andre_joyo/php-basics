<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Functions</title>
</head>
<body>
    <?php
        //Genera una función que dados dos números devuelva la suma de ambos
        function sum($a, $b){
            return $a+$b;
        }
        echo "sum: ".sum(20,5)."<br>";
        //Genera una función que dados dos números devuelva la multiplicación de ambos
        function prod($a, $b){
            return $a*$b;
        }
        echo "prod: ".prod(20,5)."<br>";
        //Genera una función que dados dos números devuelva la división de ambos
        function div($a, $b){
            return $a/$b;
        }
        echo "div: ".div(20,5)."<br>";
        //Genera una función que dado dos números y una operación ( sumar, multiplicar o dividir ) devuelva el resultado de dicha operación.
        //Dependiendo del tipo de operación recibida por parámetro, la función ejecutará a la función responsable de realizar dicha operación, ya que previamente has implementado la función para cada operación por separado.
        function oper($a, $b, $oper){
            switch ($oper) {
                case 'sum':
                    $result=sum($a,$b);
                    break;
                case 'prod':
                    $result=prod($a,$b);
                    break;
                case 'div':
                    $result=div($a,$b);
                    break;
            }
            return $result;
        }
        echo "oper: ".oper(20,5,"sum")."<br>";
    ?>    
</body>
</html>