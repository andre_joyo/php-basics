<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dates</title>
</head>
<body>
    <?php
        //Obtener la fecha actual en cualquier formato
        print_r(getdate());
        $date=getdate();
        foreach ($date as $key => $value) {
            echo "\$date[".$key."]: $value<br>";
        }
        //Obtener el día actual
        echo "dd/mm/yy: $date[mday]/$date[mon]/$date[year]<br>";
        //Obtener el mes actual en formato numérico (1-12)
        echo "month: ".$date["mon"]."<br>";
        //Obtener el minuto actual con ceros iniciales (00 - 59)
        echo "minute: 00-".$date["minutes"]."<br>";
        //Instanciar la clase DateTIme y posteriormente invocar al método format con el argumento “Y-m-d” para que muestre año-mes-dia
        $my_date = new DateTime('09-12-2019');
        echo $my_date->format('Y-m-d')."<br>";
        var_dump($my_date);
    ?>
</body>
</html>