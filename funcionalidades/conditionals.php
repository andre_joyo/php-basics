<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Conditionals</title>
</head>
<body>
    <?php
        //Crea una condición simple que evalúe si el día actual es Lunes. Sólo en caso de que la condición se cumpla muestra un mensaje de tipo “Estamos a lunes“
        $now=getdate();
        if ($now["weekday"]=="Monday") {
            echo "Estamos a Lunes <br>";
        }
        //Crea una condición simple que evalúe si el mes actual es Octubre. En caso de que la condición se cumpla muestra un mensaje de tipo “Estamos en Octubre“, por el contrario, si dicha condición no se cumple, se mostrará un mensaje de tipo “No estamos en Octubre“
        if ($now["mon"]=="10") {
            echo "Estamos en Octubre";
        } else {
            echo "No estamos en Octubre";
        }
        echo "<br>";
        /*Crea una doble condición que evalúe:
        si el minuto actual es inferior a 10
        Muestra un mensaje de tipo “el minuto actual es inferior a 10“
        si el minuto actual es superior a 15
        Muestra un mensaje de tipo “el minuto actual es superior a 15“
        si no cumple ninguna de las dos condiciones anteriores
        Muestra un mensaje de tipo “no se cumple ninguna condición“*/
        if ($now["minutes"]<10) {
            echo "El minuto actual es inferior a 10";
        } elseif($now["minutes"]>15){
            echo "El minuto actual es superior a 15";
        } else {
            echo "No se cumple ninguna condición";
        }
        echo "<br>";
        /*Crea una estructura de control de tipo switch para que se muestre un mensaje diferente 
        dependiendo del día de la semana actual. Puedes escribir cualquier tipo de mensaje, lo importante
         es que entiendas cómo funciona y en qué casos usarlo.*/
        switch ($now["weekday"]) {
            case "Monday":
                echo "Today is Monday";
                break;
            case "Twesday":
                echo "Today is Twesday";
                break;
            case "Wendsday":
                echo "Today is Wendsday";
                break;
            case "Thursday":
                echo "Today is Thursday";
                break;
            case "Friday":
                echo "Today is Friday";
                break;
            case "Saturday":
                echo "Today is Saturday";
                break;
            case "Sunnday":
                echo "Today is Sunnday";
                break;    
        }      
    ?>
</body>
</html>