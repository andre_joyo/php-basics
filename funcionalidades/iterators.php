<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Iterators</title>
</head>
<body>
    <?php
        //Genera un snippet que haga uso de for
        $fib[0]=$fib[1]=1;
        for ($i=2; $i < 25; $i++) { 
            $fib[$i]=$fib[$i-2]+$fib[$i-1];
        }
        //Genera un snippet que haga uso de foreach
        foreach ($fib as $key => $value) {
            echo "\$fib[".$key."] ".$value."<br>";
        }
        //Genera un snippet que haga uso de while
        $a=0;
        while ($a < count($fib)) {
            $a++;
        }    
        echo "Hay ".$a." elementos en fib<br>";    
        //Genera un snippet que haga uso de do while
        do {
            array_pop($fib);
        } while (count($fib)>0);
        echo "Array fib vaciado: ";
        var_dump($fib);
    ?>
</body>
</html>