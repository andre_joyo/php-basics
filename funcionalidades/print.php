<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print</title>
</head>
<body>
    <?php
    for ($i=0; $i < 5; $i++) { 
        for ($j=0; $j < 5; $j++) { 
            $arr[$i][$j]=$i*5+$j;
        }
    }
    //Genera una instrucción que haga uso de “echo”
    echo "Hola mundo con \"echo\"<br>";
    //Genera una instrucción que haga uso de “print”
    print "Hola mundo con \"print\"<br>";
    //Genera una instrucción que haga uso de “print_r”, es importante que asignes un valor complejo para analizar su potencial    
    echo "\"print_r\":<br>";
    print_r($arr);
    echo "<br>";
    echo "\"var_dump\":<br>";
    var_dump($arr)
    ?>
</body>
</html>
